"use strict";

function receiveToCreateTable() {
    var row1 = document.createElement('tr')
    var thead = document.createElement('thead')
    var tbody = document.createElement('tbody')
    var table = document.createElement('table')

    var receiver = document.getElementById('receiver').value


    var td1 = document.createElement('td')
    td1.innerHTML = "#"

    row1.appendChild(td1)
    thead.appendChild(row1)

    for (var i = 0; i < receiver; i++) {
        var td = document.createElement('td')
        var tr2 = document.createElement('tr')
        td.innerHTML = i + 1

        tr2.appendChild(td)
        tbody.appendChild(tr2)
    }

    table.appendChild(thead)
    table.appendChild(tbody)

    var tableResult = document.getElementById('tableResult')
    tableResult.appendChild(table)
}