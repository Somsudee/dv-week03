"use strict";

function showAlert(text){
    console.log(text)
}

function listFormElement(){
    var formElements = document.getElementsByTagName('form')
    console.log(formElements)
}

function copyTextFromNameToLastName(){
    var nameElement = document.getElementById('firstName')
    var surnameElement = document.getElementById('lastname')
    var firstname = nameElement.value
    console.log('firstname is '+firstname)
    console.log('now lastname is '+surnameElement.value)
    surnameElement.value = firstname
}

function reset(){
    var nameElement = document.getElementById('firstName');
    var surnameElement = document.getElementById('lastname');
    var emailElement = document.getElementById('Email');
    var password1Element = document.getElementById('Password1');
    var password2Element = document.getElementById('Password2');
    var radio1Element = document.getElementById('Radios1');
    var radio2Element = document.getElementById('Radios2');
    var checkboxElement = document.getElementById('Checkbox1');

    nameElement.value = "";
    surnameElement.value = "";
    emailElement.value = "";
    password1Element.value = "";
    password2Element.value = "";
    radio1Element.value = "";
    radio2Element.value = "";
    checkboxElement.value = "";


}

function getValueRadio(){
    var emailbox = document.getElementById('Email')
    var status = document.getElementById('studentStatus')

    if (document.getElementById('Radios1').checked) {
        status = document.getElementById('Radios1').value;
    }else if (document.getElementById('Radios2').checked) {
        status = document.getElementById('Radios2').value;
    }else{
        status = ""
    }
    emailbox.value = status
}

function showSummary(){
    var name = document.getElementById('firstName').value
    var surname = document.getElementById('lastname').value
    var email = document.getElementById('Email').value
    var password1 = document.getElementById('Password1').value
   
    var summaryElement = document.getElementById('summary')

    var status = document.getElementById('studentStatus')

    if (document.getElementById('Radios1').checked) {
        status = document.getElementById('Radios1').value;
    }else if (document.getElementById('Radios2').checked) {
        status = document.getElementById('Radios2').value;
    }else{
        status = ""
    }
    summaryElement.innerHTML = "Name: "+ name + "<br>" + "Surname: "+ surname + "<br>" +"Email: "+ email + "<br>" +"Password: "+ password1 + "<br>" +"Student status: "+ status   

    if(name == "" && surname=="" && email=="" && password1=="" && status == ""){
        summaryElement.setAttribute('hidden','true')
    }else {
        summaryElement.removeAttribute('hidden')
    }
}

function addElement(){
    var node = document.createElement('p')
    node.innerHTML = 'Hello World'
    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(node)
}

function addHref(){
    var node = document.createElement('p')
    node.innerHTML = 'google'
    node.setAttribute('href','https://www.google.com')
    node.setAttribute('target', '_blank')
    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(node)
}

function addBoth(){
    var node = document.createElement('p')
    node.innerHTML = 'Hello World'
    var node2 = document.createElement('p')
    node2.innerHTML = 'google'
    node2.setAttribute('href','https://www.google.com')
    node2.setAttribute('target', '_blank')
    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(node)
    placeholder.appendChild(node2)
}

function addForm(){
    var node  = document.createElement('p')
    node.innerHTML = 'Please fill in this form to create account'

    var node2 = document.createElement('input')
    node2.innerHTML = ''
    node2.setAttribute('type','text')
    node2.setAttribute('class','form-control')
    node2.setAttribute('id','firstName')
    node2.setAttribute('placeholder','First name')

    var node3 = document.createElement('input')
    node3.innerHTML = ''
    node3.setAttribute('type','text')
    node3.setAttribute('class','form-control')
    node3.setAttribute('id','lastName')
    node3.setAttribute('placeholder','Last name')

    var node4 = document.createElement('input')
    node4.innerHTML = ''
    node4.setAttribute('type','email')
    node4.setAttribute('class','form-control')
    node4.setAttribute('id','email')
    node4.setAttribute('aria-describedby','emailHelp')
    node4.setAttribute('placeholder','email')

    /*div*/
    var divFormGroup = document.createElement('div')
    divFormGroup.setAttribute('class','form-group')
    divFormGroup.appendChild(node2)

    var divFormGroup2 = document.createElement('div')
    divFormGroup2.setAttribute('class','form-group')
    divFormGroup2.appendChild(node3)

    var divFormGroup3 = document.createElement('div')
    divFormGroup3.setAttribute('class','form-group')
    divFormGroup3.appendChild(node4)

    /*col*/
    var colName = document.createElement('div')
    colName.setAttribute('class','col-6')
    colName.appendChild(divFormGroup)

    var colSurname = document.createElement('div')
    colSurname.setAttribute('class','col-6')
    colSurname.appendChild(divFormGroup2)
    
    var colEmail = document.createElement('div')
    colEmail.setAttribute('class','col-12')
    colEmail.appendChild(divFormGroup3)

    /*row*/
    var rowName = document.createElement('div')
    rowName.setAttribute('class','row')
    rowName.appendChild(colName)
    rowName.appendChild(colSurname)

    var rowEmail = document.createElement('div')
    rowEmail.setAttribute('class','row')
    rowEmail.appendChild(colEmail)

    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(node)
    placeholder.appendChild(rowName)
    placeholder.appendChild(rowEmail)

}

function addTable(){
    var th = document.createElement('th')
    th.innerHTML = '#'
    th.setAttribute('scope','col')

    var th2 = document.createElement('th')
    th2.innerHTML = 'First'
    th2.setAttribute('scope','col')

    var th3 = document.createElement('th')
    th3.innerHTML = 'Last'
    th3.setAttribute('scope','col')

    var th4 = document.createElement('th')
    th4.innerHTML = 'Handle'
    th4.setAttribute('scope','col')

/*1 */
    var th5 = document.createElement('th')
    th5.innerHTML = '1'
    th5.setAttribute('scope','row')

    var td = document.createElement('td')
    td.innerHTML = 'Mark'

    var td2 = document.createElement('td')
    td2.innerHTML = 'Otto'

    var td3 = document.createElement('td')
    td3.innerHTML = '@mdo'

/*2 */
    var th6 = document.createElement('th')
    th6.innerHTML = '2'
    th6.setAttribute('scope','row')

    var td4 = document.createElement('td')
    td4.innerHTML = 'Jacod'

    var td5 = document.createElement('td')
    td5.innerHTML = 'Thornton'

    var td6 = document.createElement('td')
    td6.innerHTML = '@fat'

/* 3 */
    var th7 = document.createElement('th')
    th7.innerHTML = '3'
    th7.setAttribute('scope','row')

    var td7 = document.createElement('td')
    td7.innerHTML = 'Larry'

    var td8 = document.createElement('td')
    td8.innerHTML = 'the Bird'

    var td9 = document.createElement('td')
    td9.innerHTML = '@twitter'

/* tr */
    var tr = document.createElement('tr')
    tr.appendChild(th)
    tr.appendChild(th2)
    tr.appendChild(th3)
    tr.appendChild(th4)

    var tr2 = document.createElement('tr')
    tr2.appendChild(th5)
    tr2.appendChild(td)
    tr2.appendChild(td2)
    tr2.appendChild(td3)

    var tr3 = document.createElement('tr')
    tr3.appendChild(th6)
    tr3.appendChild(td4)
    tr3.appendChild(td5)
    tr3.appendChild(td6)

    var tr4 = document.createElement('tr')
    tr4.appendChild(th7)
    tr4.appendChild(td7)
    tr4.appendChild(td8)
    tr4.appendChild(td9)

/* thead & tbody */
    var thead = document.createElement('thead')
    thead.appendChild(tr)

    var tbody = document.createElement('tbody')
    tbody.appendChild(tr2)
    tbody.appendChild(tr3)
    tbody.appendChild(tr4)

/* table */
    var table = document.createElement('table')
    table.appendChild(thead)
    table.appendChild(tbody)

    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(table)
}
