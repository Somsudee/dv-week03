function loadAllMovies() {
    var responseData = null;
    var data = fetch('https://dv-excercise-backend.appspot.com/movies')
        .then((response) => {
            console.log(response)
            return response.json()
        }).then((json) => {
            responseData = json
            var resultElement = document.getElementById('result')
            resultElement.innerHTML = JSON.stringify(json, null, 2)
        })
}

/* asyn */
async function loadAllMoviesAsync() {
    let response = await fetch('https://dv-excercise-backend.appspot.com/movies')
    let data = await response.json()
    return data

}

/* create table */
function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.innerHTML =''
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    //create the header
    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        //add data 
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope', 'row')
            dataFirstColumnNode.innerText = currentData['name']
            dataRow.appendChild(dataFirstColumnNode)

            var columnNode = null;
            columnNode = document.createElement('td')
            columnNode.innerText = currentData['synopsis']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)
        }
    })
}


async function loadOneMovie(){
    let name = document.getElementById('queryId').value
    if(name != '' && name !=null){
        let response = await fetch('https://dv-excercise-backend.appspot.com/movies/'+name)
        return await response.json()

    }
}